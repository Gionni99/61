/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.HashMap;

/**
 *
 * @author Giovanni Bandeira
 */
public class Time {
    private HashMap<String,Jogador> jogadores;

    public Time() {
        this.jogadores = new HashMap<>();
    }

    public HashMap getJogadores() {
        return jogadores;
    }
    
    public void addJogador(String x,Jogador y){
        this.jogadores.put(x,y);
    }
}