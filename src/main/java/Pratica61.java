
import utfpr.ct.dainf.if62c.pratica.Time;
import utfpr.ct.dainf.if62c.pratica.Jogador;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica61 {
    public static void main(String[] args) {
        Time Palmera,Mengao;
        Palmera = new Time();
        Mengao = new Time();
        
        Jogador a1,a2,a3,b1,b2,b3;
        a1=new Jogador(18,"Rodisvaldo");
        a2=new Jogador(23,"Renatinho");
        a3=new Jogador(83,"Lewandowski");
        b1=new Jogador(11,"Julio");
        b2=new Jogador(98,"Breda");
        b3=new Jogador(13,"Morgan");
        
        Palmera.addJogador("Goleiro",a1);
        Palmera.addJogador("Zagueiro",a2);
        Palmera.addJogador("Atacante",a3);
        Mengao.addJogador("Goleiro", b1);
        Mengao.addJogador("Zagueiro",b2);
        Mengao.addJogador("Atacante",b3);
        
        System.out.println("Posição    Palmera             Mengao");
        System.out.println("Goleiro    "+Palmera.getJogadores().get("Goleiro").toString()+"        "+Mengao.getJogadores().get("Goleiro").toString());
        System.out.println("Zagueiro   "+Palmera.getJogadores().get("Zagueiro").toString()+"        "+Mengao.getJogadores().get("Zagueiro").toString());
        System.out.println("Atacante:  "+Palmera.getJogadores().get("Zagueiro").toString()+"        "+Mengao.getJogadores().get("Atacante").toString());
        
    
    } 
}
